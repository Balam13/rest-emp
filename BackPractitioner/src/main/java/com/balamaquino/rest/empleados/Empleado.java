package com.balamaquino.rest.empleados;

import java.util.ArrayList;

public class Empleado {
    private Integer ide;
    private String nombre;
    private String apellido;
    private String email;
    private ArrayList<Capacitacion> capacitaciones;

    public Empleado() {
    }

    public Empleado(Integer ide, String nombre, String apellido, String email, ArrayList <Capacitacion> capacitaciones) {
        this.ide = ide;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.capacitaciones=capacitaciones;

    }

    public ArrayList<Capacitacion> getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(ArrayList<Capacitacion> capacitaciones) {
        this.capacitaciones = capacitaciones;
    }
    public Integer getIde() { return ide;    }
    public void setIde(Integer ide) {
        this.ide = ide;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }




    @Override
    public String toString() {
        return "Empleado{" +
                "ide=" + ide +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", email='" + email + '\'' +
                '}';
    }


}
