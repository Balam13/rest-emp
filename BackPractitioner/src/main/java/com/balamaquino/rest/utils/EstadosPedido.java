package com.balamaquino.rest.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINADO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO;
}
