package com.balamaquino.rest.repositorios;

import com.balamaquino.rest.empleados.Capacitacion;
import com.balamaquino.rest.empleados.Empleado;
import com.balamaquino.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadoDAO
{
    Logger logger= LoggerFactory.getLogger(EmpleadoDAO.class);
    private static Empleados list = new Empleados();
    static
    {
        Capacitacion cap1= new Capacitacion("2020/01/01", "DBA");
        Capacitacion cap2= new Capacitacion("2019/01/01", "Back End");
        Capacitacion cap3= new Capacitacion("2018/01/01", "Front End");
        ArrayList<Capacitacion> una =new ArrayList<Capacitacion>();
        una.add(cap1);
        ArrayList<Capacitacion> dos =new ArrayList<Capacitacion>();
        dos.add(cap1);
        dos.add(cap2);
        ArrayList<Capacitacion> todas =new ArrayList<Capacitacion>();
        todas.add(cap3);
        todas.addAll(dos);
        list.getListaEmpleados().add(new Empleado(1,"Antonio","Lopez","antonio@lopez.com",una));
        list.getListaEmpleados().add(new Empleado(2,"Pedro","Perez","pedro@perez.com",dos));
        list.getListaEmpleados().add(new Empleado(3,"Luis","Marquez","luis@marquez.com",todas));
    }
    public Empleados getAllEmpleados()
    {
        logger.debug("Empleados devueltos");

        return list;
    }
    public Empleado getEmpleado(int id)
    {
        for (Empleado emp:list.getListaEmpleados())
        {
            if(emp.getIde()==id)
            {
                return emp;
            }
        }
        return null;
    }
    public void addEmpleado(Empleado emp)
    {
        list.getListaEmpleados().add(emp);
    }
    public void updEmpleado(Empleado emp)
    {
        Empleado current = getEmpleado(emp.getIde());
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());;
        current.setEmail(emp.getEmail());
    }
    public void updEmpleado(int id, Empleado emp)
    {
        Empleado current = getEmpleado(id);
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());;
        current.setEmail(emp.getEmail());
    }
    public String deleteEmpleado(int id)
    {
        Empleado current = getEmpleado(id);
        if (current == null) return "Not Found";
        Iterator it= list.getListaEmpleados().iterator();
        while(it.hasNext())
        {
            Empleado emp= (Empleado) it.next();
            if (emp.getIde()==id)
            {
                it.remove();
                break;
            }
        }
        return "OK";
    }
    public void softupdEmpleado(int id, Map<String, Object> updates)
    {
        Empleado current = getEmpleado(id);
        for (Map.Entry<String, Object> update : updates.entrySet())
        {
            switch(update.getKey())
            {
                case "nombre":
                    current.setNombre(update.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido(update.getValue().toString());
                    break;
                case "email":
                    current.setEmail(update.getValue().toString());
                    break;
            }
        }
    }
    public List<Capacitacion> getCapacitacionesEmpleado(int ide)
    {
        Empleado current = getEmpleado(ide);
        ArrayList<Capacitacion> caps =new ArrayList<Capacitacion>();
        if (current != null) caps=current.getCapacitaciones();
        return caps;
    }

    public Boolean addCapacitacion(int ide, Capacitacion cap)
    {
        Empleado current = getEmpleado(ide);
        if (current==null) return false;
        current.getCapacitaciones().add(cap);
        return true;
    }

}

