package com.balamaquino.rest;

import com.balamaquino.rest.empleados.Capacitacion;
import com.balamaquino.rest.empleados.Empleado;
import com.balamaquino.rest.empleados.Empleados;
import com.balamaquino.rest.repositorios.EmpleadoDAO;
import com.balamaquino.rest.utils.Configuracion;
import com.balamaquino.rest.utils.EstadosPedido;
import com.balamaquino.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {
    @Autowired
    private EmpleadoDAO empDAO;

    @GetMapping(path = "/")
    public Empleados getEmpleados() {
        return empDAO.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id) {
        Empleado emp = empDAO.getEmpleado(id);

        if (emp == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(emp);
        }
    }

    @PostMapping(path = "/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empDAO.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setIde(id);
        empDAO.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getIde())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp) {
        empDAO.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{ide}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int ide, @RequestBody Empleado emp) {
        empDAO.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{ide}")
    public ResponseEntity<Object> delEmpleadoXId(@PathVariable int ide) {
        String resp = empDAO.deleteEmpleado(ide);
        if (resp == "OK") {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @PatchMapping(path = "/{ide}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softupdEmpleado(@PathVariable int ide, @RequestBody Map<String, Object> updates) {
        empDAO.softupdEmpleado(ide, updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{ide}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int ide) {
        return ResponseEntity.ok().body(empDAO.getCapacitacionesEmpleado(ide));
    }

    @PostMapping(path = "/{ide}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int ide, @RequestBody Capacitacion cap) {
        if (empDAO.addCapacitacion(ide, cap)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.noContent().build();
        }
    }
    @Value("${app.titulo}") private String titulo;
    @GetMapping("/titulo")
    public String getAppTitulo()
    {
        String modo=configuracion.getModo();
        return String.format("%s (%s)",titulo,modo);
    }
    @Autowired
    private Environment env;
    @GetMapping("/autor")
    public String getAppAutor()
    {
        return env.getProperty("app.autor");
    }

    @Autowired
    Configuracion configuracion;

    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador) {
        try {

            return Utilidades.getCadena(texto, separador);
        }catch(Exception e){
            return "";
        }
    }



}
