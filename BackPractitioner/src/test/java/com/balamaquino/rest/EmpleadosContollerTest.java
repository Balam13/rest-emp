package com.balamaquino.rest;

import com.balamaquino.rest.utils.BadSeparator;
import com.balamaquino.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class EmpleadosContollerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena()
    {
        String correcto = "L.U.Z.D.E.L.S.O.L";
        String origen= "Luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen, "."));
    }
    @Test
    public void testSeparadorGetCadena()
    {
        try {
            Utilidades.getCadena("balam Aquino","..");
            fail("Se esperaba BadSeparator");
        }catch(BadSeparator vs){}
    }

    @Test
    public void testgetAutor()
    {
        assertEquals("Balam Aquino", empleadosController.getAppAutor());
    }
    @ParameterizedTest
    @ValueSource(ints= {1,3,5,15,-12, Integer.MAX_VALUE})
    public void testEsImpar(int numero)
    {
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings= {""," "})
    public void testEstaBlanco(String texto)
    {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings= {" ","\t", "\n" })
    public void testEstaBlancoCompleto(String texto)
    {
        assertTrue(Utilidades.estaBlanco(texto));
    }

}
